// JavaScript Document

$(function () {
        //要素設定
        var tab = '#tab li';
        var tabSelectImg = '#tab li.selected img';
        var tabBox = '#tabBox > div';
        var selected = 'selected';
        //初期表示
        $(tabBox).hide();
        if (location.hash) {
                $(location.hash).show();
                $('#tab li.' + location.hash.substr(1)).addClass(selected);
                $(tabSelectImg).attr('src', $(tabSelectImg).attr('src').replace('_of.', '_on.'));
        } else {
                $(tab).first().addClass(selected);
                $(tabSelectImg).attr('src', $(tabSelectImg).attr('src').replace('_of.', '_on.'));
                $(tabBox).first().show();
        }
        //クリックイベント
        $(tab).click(function () {
                $(tabSelectImg).attr('src', $(tabSelectImg).attr('src').replace('_on.', '_of.'));
                $(tab).removeClass(selected);
                $(this).addClass(selected);
                $(tabSelectImg).attr('src', $(tabSelectImg).attr('src').replace('_of.', '_on.').replace('_ov.', '_on.'));
                $(tabBox).hide();
                var selectTab = $(this).find('a').attr('href');
                location.hash = selectTab;
                $(selectTab).fadeIn();
                return false;
        });
        //タブ ホバー イベント
        $(tab).hover(function () {
                $('img', this).attr('src', $('img', this).attr('src').replace('_of.', '_ov.'));
        }, function () {
                $('img', this).attr('src', $('img', this).attr('src').replace('_ov.', '_of.'));
        });
});