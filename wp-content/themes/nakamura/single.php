<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>


<div id="MainArea">

<div id="Crumbs"><a href="<?php bloginfo('url'); ?>">Home</a> &gt;
	<?php $cat = get_the_category(); echo get_category_parents($cat[0], true, ' &gt; '); ?>
	<?php the_title(); ?></div>

<div class="Figure">
  <?php include (TEMPLATEPATH . '/title-single.php'); ?>
</div>
</div>

<div id="Contents">
<div class="Article">
<div class="PageMain">

<div class="SectionL">


<?php include (TEMPLATEPATH . '/title-single2.php'); ?>


<div class="NewsSection">

<div id="container">
<div id="content" role="main">

<?php #ループ開始 ?>

<?php if ( in_category('4') ) : ?>

<?php include (TEMPLATEPATH . '/single-voice.php'); ?>
	</div><!-- #content -->
	</div><!-- #container -->
</div>
<div class="Back"><a href="<?php echo home_url( '/' ); ?>/voice"><img src="<?php bloginfo( 'template_url' ); ?>/images/voice/common/back.jpg" alt="お客様の声一覧に戻る" width="150" height="17"></a></div>
<br>
<div id="form_com">
<h4><img src="<?php bloginfo( 'template_url' ); ?>/images/voice/common/title_form01.jpg" ></h4>
<p class="form_p">など、HPには掲載されていないお客様様の声の希望がございましたら下記からリクエストしてください。<br>
当社で建てさせていただいた数多くの施主様のご感想から、お客様のご希望に沿う資料をセレクトしてお送りさせていただきます。</p>

<h4><img src="<?php bloginfo( 'template_url' ); ?>/images/voice/common/title_form02.jpg" alt="お客様の声リクエストフォーム"></h4>
<?php echo do_shortcode('[contact-form-7 id="3613" title="お客様の声リクエストフォーム"]'); ?>
</div>

</div>
<?php elseif ( in_category('5') ) : ?>
<?php include (TEMPLATEPATH . '/single-works.php'); ?>
	</div><!-- #content -->
	</div><!-- #container -->
</div>
<div class="Back"><a href="<?php echo home_url( '/' ); ?>/works"><img src="<?php bloginfo( 'template_url' ); ?>/images/works/common/back.jpg" alt="施工事例一覧に戻る" width="150" height="17"></a></div>
<br>
<div id="form_com">
<h4><img src="<?php bloginfo( 'template_url' ); ?>/images/works/common/title_form01.jpg" ></h4>

<p class="form_p">など、HPには掲載されていない事例の希望がございましたら下記からリクエストしてください。<br>
当社が過去に手掛けた数多くの事例から、お客様のご希望に沿う事例紹介資料をセレクトしてお送りさせていただきます。</p>

<h4><img src="<?php bloginfo( 'template_url' ); ?>/images/works/common/title_form02.jpg" alt="施工事例資料リクエストフォーム"></h4>

<?php echo do_shortcode('[contact-form-7 id="3614" title="施工事例リクエストフォーム"]'); ?>
</div>
</div>
<?php elseif ( in_category('6') ) : ?>
<?php include (TEMPLATEPATH . '/single-land.php'); ?>
	</div><!-- #content -->
	</div><!-- #container -->
</div>
<div class="Back"><a href="<?php echo home_url( '/' ); ?>/land"><img src="<?php bloginfo( 'template_url' ); ?>/images/land/common/back.jpg" alt="土地情報情報一覧に戻る" width="132" height="17"></a></div>
<br>
<div id="form_com">
<h4><img src="<?php bloginfo( 'template_url' ); ?>/images/land/common/title_form01.jpg" ></h4>

<p class="form_p">など、HPには掲載されていない土地情報の希望がございましたら下記からリクエストしてください。<br>
当社と提携している不動産会社のネットワークからお客様のご希望に沿う資料をセレクトしてお送りさせていただきます。<br>
</p>

<h4><img src="<?php bloginfo( 'template_url' ); ?>/images/land/common/title_form02.jpg" alt="土地情報リクエストフォーム"></h4>

<?php echo do_shortcode('[contact-form-7 id="3612" title="土地情報リクエストフォーム"]'); ?>
</div>
</div>
<?php elseif ( in_category('3') ) : ?>

<?php
/* Run the loop to output the post.
* If you want to overload this in a child theme then include a file
 * called loop-single.php and that will be used instead.
*/
get_template_part( 'loop', 'single' );
?>
	</div><!-- #content -->
	</div><!-- #container -->
</div>
<div class="Back"><a href="<?php echo home_url( '/' ); ?>/news"><img src="<?php bloginfo( 'template_url' ); ?>/images/new/common/back.jpg" alt="イベント＆新着情報一覧に戻る" width="184" height="17"></a></div>
</div>

<?php else : ?>
	</div><!-- #content -->
	</div><!-- #container -->
</div>
<div class="Back"><a href="<?php echo home_url( '/' ); ?>/news"><img src="<?php bloginfo( 'template_url' ); ?>/images/new/common/back.jpg" alt="イベント＆新着情報一覧に戻る" width="184" height="17"></a></div>
</div>
<?php endif; ?>




<?php get_sidebar(); ?>

<div class="clear"></div>
</div>
<p class="PageUp2"><a href="#top"><img src="<?php bloginfo( 'template_url' ); ?>/images/common/parts/up_off.jpg" alt="ページトップ" width="88" height="11"></a></p>
</div>
</div>







<?php get_footer(); ?>
