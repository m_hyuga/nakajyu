<?php
/**
 * The loop that displays a single post.
 *
 * The loop displays the posts and the post content.  See
 * http://codex.wordpress.org/The_Loop to understand it and
 * http://codex.wordpress.org/Template_Tags to understand
 * the tags used in it.
 *
 * This can be overridden in child themes with loop-single.php.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.2
 */
?>


<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
<div class="Works-Section">


<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<h3 class="entry-title"><?php the_title(); ?> <?php echo c2c_get_custom('voice-txt02'); ?></h3>
    
	
<div class="Main-Photo">

                           
<div class="entry-content">
	<?php the_content(); ?>
      <br class="clear" />
</div><!-- .entry-content -->

</div>
<!-- 一列目 -->
<div class="Main-Photo">
<div class="Main-PhotoL">

<?php
$imagefield = get_imagefield('works-photo01');
$attachment = get_attachment_object($imagefield['id']);

if (empty($imagefield['id'])) {
echo '';
} else {
        echo '<image src="' . $attachment['url'] . '" alt="" width="320" />';
		}
?>
<?php echo c2c_get_custom('works-title01','<h4>','</h4>'); ?>
<?php echo c2c_get_custom('works-txt01','<p>','</p>'); ?>
</div>

<div class="Main-PhotoR">
<?php
$imagefield = get_imagefield('works-photo02');
$attachment = get_attachment_object($imagefield['id']);

if (empty($imagefield['id'])) {  
echo '';
} else {
        echo '<image src="' . $attachment['url'] . '" alt="" width="320" />';
		}
?>
<?php echo c2c_get_custom('works-title02','<h4>','</h4>'); ?>
<?php echo c2c_get_custom('works-txt02','<p>','</p>'); ?>
</div>
  <br class="clear">
</div>
<!-- 一列目ここまで -->





<!-- 二列目 -->
<div class="Main-Photo">
<div class="Main-PhotoL">

<?php
$imagefield = get_imagefield('works-photo03');
$attachment = get_attachment_object($imagefield['id']);

if (empty($imagefield['id'])) {
echo '';
} else {
        echo '<image src="' . $attachment['url'] . '" alt="" width="320" />';
		}
?>
<?php echo c2c_get_custom('works-title03','<h4>','</h4>'); ?>
<?php echo c2c_get_custom('works-txt03','<p>','</p>'); ?>
</div>

<div class="Main-PhotoR">
<?php
$imagefield = get_imagefield('works-photo04');
$attachment = get_attachment_object($imagefield['id']);

if (empty($imagefield['id'])) {  
echo '';
} else {
        echo '<image src="' . $attachment['url'] . '" alt="" width="320" />';
		}
?>
<?php echo c2c_get_custom('works-title04','<h4>','</h4>'); ?>
<?php echo c2c_get_custom('works-txt04','<p>','</p>'); ?>
</div>
  <br class="clear">
</div>
<!-- 二列目ここまで -->
   




<!-- 三列目 -->
<div class="Main-Photo">
<div class="Main-PhotoL">

<?php
$imagefield = get_imagefield('works-photo05');
$attachment = get_attachment_object($imagefield['id']);

if (empty($imagefield['id'])) {
echo '';
} else {
        echo '<image src="' . $attachment['url'] . '" alt="" width="320" />';
		}
?>
<?php echo c2c_get_custom('works-title05','<h4>','</h4>'); ?>
<?php echo c2c_get_custom('works-txt05','<p>','</p>'); ?>
</div>

<div class="Main-PhotoR">
<?php
$imagefield = get_imagefield('works-photo06');
$attachment = get_attachment_object($imagefield['id']);

if (empty($imagefield['id'])) {  
echo '';
} else {
        echo '<image src="' . $attachment['url'] . '" alt="" width="320" />';
		}
?>
<?php echo c2c_get_custom('works-title06','<h4>','</h4>'); ?>
<?php echo c2c_get_custom('works-txt06','<p>','</p>'); ?>
</div>
  <br class="clear">
</div>
<!-- 三列目ここまで -->



</div><!-- #post-## -->
	</div>
					<div class="entry-utility">
						
						<?php edit_post_link( __( 'Edit', 'twentyten' ), '<span class="edit-link">', '</span>' ); ?>
					</div><!-- .entry-utility -->
			

		
<?php endwhile; // end of the loop. ?>
