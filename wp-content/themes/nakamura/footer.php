<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content
 * after.  Calls sidebar-footer.php for bottom widgets.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?>
<div id="Footer">
<div id="FooterInner">
<div class="Footer clearfix">
<h1 class="left"><img src="<?php bloginfo( 'template_url' ); ?>/images/common/footer/logo.jpg"></h1>
<a href="<?php echo home_url( '/' ); ?>form/"><img src="<?php bloginfo( 'template_url' ); ?>/images/common/footer/button.jpg" class="button"></a>
</div>
</div>
<div class="Footer clearfix">
<div class="nav">
<ul>
<li><a href="<?php echo home_url( '/' ); ?>">ホーム</a></li>
<li><a href="<?php echo home_url( '/' ); ?>/other/recruit.html">採用情報</a></li>
<li><a href="<?php echo home_url( '/' ); ?>/other/sitemap.html">サイトマップ</a></li>
<li><a href="<?php echo home_url( '/' ); ?>/other/privacypolicy.html">プライバシーポリシー</a></li>
</ul>
</div>
<address>
<img src="<?php bloginfo( 'template_url' ); ?>/images/common/footer/copy.jpg">
</address>
</div>
</div>
</div>

<?php
	/* Always have wp_footer() just before the closing </body>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to reference JavaScript files.
	 */

	wp_footer();
?>
<script type='text/javascript' src='<?php bloginfo("stylesheet_directory") ?>/ajaxzip2/ajaxzip2.js'></script>
 <script type="text/javascript">
 //<![CDATA[
 jQuery(function(){
 AjaxZip2.JSONDATA = "<?php bloginfo("stylesheet_directory") ?>/ajaxzip2/data";
 jQuery('#your-zip').keyup(function(event){
 AjaxZip2.zip2addr(this,'your-pref','your-addr');
 })
 })
 //]]>
 </script> 
<?php //サンクスページ振り分け
if(is_page( array( 3615,3618,3622,3599,3606,3124 ) ) ): ?>
<script type="text/javascript" language="javascript">
  /* <![CDATA[ */
  var yahoo_ydn_conv_io = "FTJbNdcOLDUU9D5kOC0C";
  var yahoo_ydn_conv_label = "";
  var yahoo_ydn_conv_transaction_id = "";
  var yahoo_ydn_conv_amount = "0";
  /* ]]> */
</script>
<script type="text/javascript" language="javascript" charset="UTF-8" src="//b90.yahoo.co.jp/conv.js"></script>
<!-- Yahoo Code for your Conversion Page -->

<script type="text/javascript">
/* <![CDATA[ */
var yahoo_conversion_id = 1000097815;
var yahoo_conversion_label = "kuOdCOqV8AgQvs2O1QM";
var yahoo_conversion_value = 0;
/* ]]> */
</script>
<script type="text/javascript" src="http://i.yimg.jp/images/listing/tool/cv/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="http://b91.yahoo.co.jp/pagead/conversion/1000097815/?value=0&amp;label=kuOdCOqV8AgQvs2O1QM&amp;guid=ON&amp;script=0&amp;disvt=true"/>
</div>
</noscript>
<!-- Google Code for &#21839;&#12356;&#21512;&#12431;&#12379;&#23436;&#20102; Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 975882278;
var google_conversion_language = "en";
var google_conversion_format = "2";
var google_conversion_color = "ffffff";
var google_conversion_label = "hhS_COLUtAcQppCr0QM";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/975882278/?label=hhS_COLUtAcQppCr0QM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

<?php endif; ?>
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 975882278;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/975882278/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
<script type="text/javascript" language="javascript">
/* <![CDATA[ */
var yahoo_retargeting_id = 'YU3Y3HYDR3';
var yahoo_retargeting_label = '';
/* ]]> */
</script>
<script type="text/javascript" language="javascript" src="//b92.yahoo.co.jp/js/s_retargeting.js"></script>
</body>
</html>
