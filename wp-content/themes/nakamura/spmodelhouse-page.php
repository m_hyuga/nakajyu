<?php
/**
 * Template Name: spmodelhouse-page
 *
 * A custom page template without sidebar.
 *
 * The "Template Name:" bit above allows this to be selectable
 * from a dropdown menu on the edit page screen.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

<div id="MainArea">
<div id="Crumbs"><a href="<?php bloginfo('url'); ?>">Home</a> &gt; <a href="<?php bloginfo('url'); ?>/modelhouse/">モデルハウスを見に行こう</a> &gt; <?php the_title(); ?></div>
</div>

<!--メインコンテンツ-->
<div id="Contents">
<div class="Article">
<div class="PageMain">

<div class="SectionL"> 
<div class="ModelArea">

			<?php
			/* Run the loop to output the page.
			 * If you want to overload this in a child theme then include a file
			 * called loop-page.php and that will be used instead.
			 */
			get_template_part( 'loop', 'page' );
			?>

</div><!-- .ModelArea -->


</div>

<div class="SectionR">

<div class="SideNav model_sidenavs">
  <img src="../images/common/side/mtop.jpg" height="18" width="270">
  <h2><img src="../images/model/common/title.jpg" alt="モデルハウスを見に行こう" width="250" height="45"></h2> <ul>
  <li><a href="<?php bloginfo('url'); ?>/nagasakamodel"><img src="../images/model/common/nav10_off.jpg" alt="長坂モデル" width="250" height="29"></a></li>
  <li><a href="<?php bloginfo('url'); ?>/nspremiumstage"><img src="../images/model/common/nav09_off.jpg" alt="西都モデル NS Premium Stage" width="250" height="29"></a></li>
    </ul>
  <img src="../images/common/side/mbottom.jpg" height="10" width="270">
</div>


<a href="<?php bloginfo('url'); ?>/works"><img src="../images/common/side/ban01.jpg" alt="施工事例集" width="270" height="95" class="MB5"></a>
<a href="<?php bloginfo('url'); ?>/voice"><img src="../images/common/side/ban02.jpg" alt="お客様の声" width="270" height="95" class="MB5"></a>
<a href="<?php bloginfo('url'); ?>/modelhouse"><img src="../images/common/side/ban03.jpg" alt="モデルハウス" width="270" height="95" class="MB5"></a>
<a href="http://nakamurajutaku.blog.fc2.com/" target="_blank"><img src="../images/common/side/ban04.jpg" alt="スタッフブログ" width="270" height="95" class="MB5"></a>
<a href="../land"><img src="../images/common/side/ban05.jpg" alt="土地探し" width="270" height="95" class="MB5"></a>
<a href="../concept"><img src="../images/common/side/ban06.jpg" alt="コンセプト" width="270" height="95"></a>
</div>

<div class="clear"></div>
</div>
<p class="PageUp2"><a href="#top"><img src="../images/common/parts/up_off.jpg" alt="ページトップ" width="88" height="11"></a></p>
</div>
</div>
<?php get_footer(); ?>
