<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<title><?php
	/*
	 * Print the <title> tag based on what is being viewed.
	 */
	global $page, $paged;

	wp_title( '|', true, 'right' );

	// Add the blog name.
	bloginfo( 'name' );


	?>┃石川県金沢市の注文住宅</title>
<link href="<?php bloginfo( 'template_url' ); ?>/css/style.css" rel="stylesheet" type="text/css" media="all">
<link href="<?php bloginfo( 'template_url' ); ?>/css/common.css" rel="stylesheet" type="text/css" media="all">

<?php  if(is_home()) : // ホームなら  ?>
<link href="<?php bloginfo( 'template_url' ); ?>/css/index.css" rel="stylesheet" type="text/css" media="all">
<?php else : ?>
<link href="<?php bloginfo( 'template_url' ); ?>/css/page02.css" rel="stylesheet" type="text/css" media="all">
<?php endif;	// is_home() ?>
<script src="<?php bloginfo( 'template_url' ); ?>/js/smartRollover.js"></script>

<!--[if IE 6]>
<script src="js/DD_belatedPNG.js"></script>
<script>
DD_belatedPNG.fix('img, .png_bg');
</script>
<![endif]-->
</head>
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php
	/* We add some JavaScript to pages with the comment form
	 * to support sites with threaded comments (when in use).
	 */
	if ( is_singular() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );

	/* Always have wp_head() just before the closing </head>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to add elements to <head> such
	 * as styles, scripts, and meta tags.
	 */
	wp_head();
?>
<? wp_enqueue_script('jquery')  ?>
<script>

  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-9506923-1', 'auto');
  ga('send', 'pageview');

</script>
</head>

<body <?php body_class(); ?>>
<div id="Wrapper"><a name="top"></a>

<!--ヘッダ-->
<div id="Header">
<div id="HeaderInner">
<div class="Header clearfix">
<h1><a href="<?php echo home_url( '/' ); ?>">
<img src="<?php bloginfo( 'template_url' ); ?>/images/common/header/logo.jpg" alt="石川県金沢市の住宅会社 中村住宅開発" width="242" height="85"></a></h1>
<div class="tel">
<a href="/form/"><img src="<?php bloginfo( 'template_url' ); ?>/images/common/header/img02.jpg" alt="お問合せ・資料請求 076-221-7666" width="332" height="49" ></a><img src="<?php bloginfo( 'template_url' ); ?>/images/common/header/img03.jpg" alt="メンテナンスに関するお問合せはこちらから 076-260-5002" width="332" height="49" >
<div class="nav">
<ul>
<li><a href="/other/recruit.html">採用情報</a></li>
<li><a href="/other/sitemap.html">サイトマップ</a></li>
<li><a href="/other/privacypolicy.html">プライバシーポリシー</a></li>
</ul>
</div>
</div>

</div>
<div id="Nav" class="clearfix">
<ul>
<li><a href="<?php echo home_url( '/' ); ?>"><img src="<?php bloginfo( 'template_url' ); ?>/images/common/nav/nav01_off.jpg" alt="HOME" width="51" height="46"></a></li>
<li><a href="/gallery" target="_blank"><img src="<?php bloginfo( 'template_url' ); ?>/images/common/nav/nav02_off.jpg" alt="デザイン研究所" width="115" height="46" class="ML4"></a></li>
<li><a href="<?php echo home_url( '/' ); ?>works"><img src="<?php bloginfo( 'template_url' ); ?>/images/common/nav/nav03_off.jpg" alt="施工事例集" width="76" height="46" class="ML4"></a></li>
<li><a href="<?php echo home_url( '/' ); ?>voice"><img src="<?php bloginfo( 'template_url' ); ?>/images/common/nav/nav04_off.jpg" alt="お客様の声" width="114" height="46" class="ML4"></a></li>
<li><a href="<?php echo home_url( '/' ); ?>news"><img src="<?php bloginfo( 'template_url' ); ?>/images/common/nav/nav05_off.jpg" alt="イベント＆新着情報" width="113" height="46" class="ML4"></a></li>
<li><a href="<?php echo home_url( '/' ); ?>modelhouse"><img src="<?php bloginfo( 'template_url' ); ?>/images/common/nav/nav06_off.jpg" alt="モデルハウスを見に行こう" width="140" height="46" class="ML4"></a></li>
<li><a href="<?php echo home_url( '/' ); ?>land"><img src="<?php bloginfo( 'template_url' ); ?>/images/common/nav/nav07_off.jpg" alt="土地情報" width="124" height="46" class="ML4"></a></li>
<li><a href="<?php echo home_url( '/' ); ?>concept"><img src="<?php bloginfo( 'template_url' ); ?>/images/common/nav/nav08_off.jpg" alt="中村住宅開発の家づくり" width="132" height="46" class="ML4"></a></li>
<li><a href="<?php echo home_url( '/' ); ?>other/profile.html"><img src="<?php bloginfo( 'template_url' ); ?>/images/common/nav/nav09_off.jpg" alt="会社案内" width="63" height="46" class="ML4"></a></li>
</ul>
</div>
</div>
</div>