<!-- 各ページの見出しを変更 -->
<?php #ループ開始 ?>

<?php if ( in_category('4') ) : ?>
  <h1><img src="<?php bloginfo( 'template_url' ); ?>/images/voice/common/title.jpg" alt="お客様の声" width="250" height="45"></h1>
  <ul>
  <li><a href="<?php echo home_url( '/' ); ?>/voice/"><img src="<?php bloginfo( 'template_url' ); ?>/images/new/common/nav01_off.jpg" alt="一覧" width="250" height="30"></a></li>

  </ul>

<?php elseif ( in_category('5') ) : ?>
 <h2><img src="<?php bloginfo( 'template_url' ); ?>/images/works/common/title.jpg" alt="施工事例" width="250" height="45"></h2>
  <ul>
  <li><a href="<?php echo home_url( '/' ); ?>/works/"><img src="<?php bloginfo( 'template_url' ); ?>/images/new/common/nav01_off.jpg" alt="一覧" width="250" height="30"></a></li>

  </ul>

<?php elseif ( in_category('6') ) : ?>
 <h2><img src="<?php bloginfo( 'template_url' ); ?>/images/land/common/title.jpg" alt="土地情報" width="250" height="45"></h2>
  <ul>
  <li><a href="<?php echo home_url( '/' ); ?>/land/"><img src="<?php bloginfo( 'template_url' ); ?>/images/new/common/nav01_off.jpg" alt="一覧" width="250" height="30"></a></li>

  </ul>

<?php elseif ( in_category('3') ) : ?>
  <h2><img src="<?php bloginfo( 'template_url' ); ?>/images/new/common/title.jpg" alt="イベント＆新着情報" width="250" height="45"></h2>
  <ul>
  <li><a href="<?php echo home_url( '/' ); ?>/news/"><img src="<?php bloginfo( 'template_url' ); ?>/images/new/common/nav01_off.jpg" alt="一覧" width="250" height="30"></a></li>

  </ul>

<?php else : ?>

<?php endif; ?>


