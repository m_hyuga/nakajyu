<?php
/**
 * The loop that displays a single post.
 *
 * The loop displays the posts and the post content.  See
 * http://codex.wordpress.org/The_Loop to understand it and
 * http://codex.wordpress.org/Template_Tags to understand
 * the tags used in it.
 *
 * This can be overridden in child themes with loop-single.php.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.2
 */
?>


<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
<div class="VoiceSection">


				<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<h3 class="entry-title"><?php the_title(); ?> <?php echo c2c_get_custom('voice-txt02'); ?></h3>
<div class="VoiceSectionL">
					
					<div class="entry-content">
						<?php the_content(); ?>
                        <?php echo c2c_get_custom('textfield'); ?>
                    <br class="clear" />

						
					</div><!-- .entry-content -->
				</div>


   <div class="VoiceSectionR">
            <div class="VoiceSectionR-no01">
            <?php
			
            	$imagefield = get_imagefield('voice-photo01');
				$attachment = get_attachment_object($imagefield['id']);
				echo '<image src="' . $attachment['url'] . '" alt="' . $attachment['title'] . '" title="' . $attachment['content'] . '"  width="208" />';
            ?>
            </div>
            
            
     
             
             <?php
$imagefield = get_imagefield('voice-photo02');
$attachment = get_attachment_object($imagefield['id']);

if (empty($imagefield['id'])) {
       
echo '';

} else {
	 	echo '<div class="VoiceSectionR-no02">';
        echo '<image src="' . $attachment['url'] . '" alt="" width="320" />';
		echo '</div>';
		}
?>
             
             
           
             
             <?php
$imagefield = get_imagefield('voice-photo03');
$attachment = get_attachment_object($imagefield['id']);

if (empty($imagefield['id'])) {
       	echo '';

} else {
	 	echo '<div class="VoiceSectionR-no02">';
        echo '<image src="' . $attachment['url'] . '" alt="" width="320" />';
		echo '</div>';
}
?>
            
            <?php
$imagefield = get_imagefield('voice-photo04');
$attachment = get_attachment_object($imagefield['id']);

if (empty($imagefield['id'])) {
       	echo '';

} else {
	 	echo '<div class="VoiceSectionR-no02">';
        echo '<image src="' . $attachment['url'] . '" alt="" width="320" />';
		echo '</div>';
}
?>
           
            <?php
$imagefield = get_imagefield('voice-photo05');
$attachment = get_attachment_object($imagefield['id']);

if (empty($imagefield['id'])) {
	
       echo '';

} else {
	 	echo '<div class="VoiceSectionR-no02">';
        echo '<image src="' . $attachment['url'] . '" alt="" width="320" />';
		echo '</div>';
		
}
?>
            
          
            <?php
$imagefield = get_imagefield('voice-photo06');
$attachment = get_attachment_object($imagefield['id']);

if (empty($imagefield['id'])) {
	
       echo '';

} else {
	 	echo '<div class="VoiceSectionR-no02">';
        echo '<image src="' . $attachment['url'] . '" alt="" width="320" />';
		echo '</div>';
		
}
?>
            
          
            <?php
$imagefield = get_imagefield('voice-photo07');
$attachment = get_attachment_object($imagefield['id']);

if (empty($imagefield['id'])) {
	
       echo '';

} else {
	 echo '<div class="VoiceSectionR-no02">';
        echo '<image src="' . $attachment['url'] . '" alt="" width="320" />';
		echo '</div>';
		
}
?>
           
            
            
</div>

<br class="clear">



<?php
$imagefield = get_imagefield('voice-photo08');
$attachment = get_attachment_object($imagefield['id']);

if (empty($imagefield['id'])) {
	
       echo '';

} else {
	 echo '<div class="VoiceSection-b">';
        echo '<image src="' . $attachment['url'] . '" alt="" width="670" />';
		echo '</div>';
		
}
?>



<div class="Voice-bg">

<div class="Voice-bg-con">

<h4>最後にマイホームを考えている人に、一言お願いします</h4>

<p><?php echo c2c_get_custom('voice-txt03'); ?></p>
</div>
</div>



</div>
	</div><!-- #post-## -->
					<div class="entry-utility">
						
						<?php edit_post_link( __( 'Edit', 'twentyten' ), '<span class="edit-link">', '</span>' ); ?>
					</div><!-- .entry-utility -->
			
	
<?php endwhile; // end of the loop. ?>
