<?php
/**
 * The template for displaying Category Archive pages.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>


<div id="MainArea">

<div id="Crumbs"><a href="<?php bloginfo('url'); ?>">Home</a> &gt; 
	<?php $cat = get_the_category(); echo get_category_parents($cat[0], false, ''); ?></div>

<div class="Figure">
   <?php include (TEMPLATEPATH . '/title-single.php'); ?>
</div>
</div>

<div id="Contents">
<div class="Article">
<div class="PageMain">

<div class="SectionL"> 



<?php include (TEMPLATEPATH . '/title-single2.php'); ?>

	<?php #ループ開始 ?>
    
    <?php if ( in_category('4') ) : ?>
    
        <div id="Voice-index">
        <h3><img src="<?php bloginfo( 'template_url' ); ?>/images/voice/common/title03.jpg" alt="お客様の声" width="670" height="18"></h3>
    
    <?php elseif ( in_category('5') ) : ?>
        <div id="Works-Section">
  
    <?php elseif ( in_category('6') ) : ?>
        <div class="NewsSection">
    
    <?php elseif ( in_category('3') ) : ?>
     <div class="NewsSection">
    
    <?php else : ?>
    
    <?php endif; ?>





<div id="container">
<div id="content" role="main">
            
<?php #ループ開始 ?>

<?php if ( in_category('4') ) : ?>

	<?php include (TEMPLATEPATH . '/loop-voice.php'); ?>

<?php elseif ( in_category('5') ) : ?>
	<?php include (TEMPLATEPATH . '/loop-works.php'); ?>

<?php elseif ( in_category('6') ) : ?>

<div id="land-index">
<br>
<div class="land-index">
<div class="land-index02">
<h3><img src="<?php bloginfo( 'template_url' ); ?>/images/land/common/title03.jpg" alt="土地探しから始めたい" width="640" height="129"></h3>

<p>理想の我が家をイメージするとき、その立地も重要条件。中村住宅開発では、土地についてもお客様1人1人あったご提案が出来るよう、常に最新の土地情報をご提供しています。待並みとの調和を大切にした当社ならではのポリシーも合わせて、ご覧ください。</p>
<img src="<?php bloginfo( 'template_url' ); ?>/images/land/common/kanazawa.jpg" alt="金沢" width="640" height="40">
</div>

<div class="land-common">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
<?php if (have_posts()) : query_posts('cat=7'); ?>
<?php include (TEMPLATEPATH . '/loop-land.php'); ?>
<?php endif; ?>
 
  </table>
</div>
<div class="land-index02">
<br>
<br>
<img src="<?php bloginfo( 'template_url' ); ?>/images/land/common/nonoichi.jpg" alt="野々市" width="640" height="40">
</div>
<div class="land-common">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
<?php if (have_posts()) : query_posts('cat=8'); ?>
<?php include (TEMPLATEPATH . '/loop-land.php'); ?>
<?php endif; ?>
 
  </table>
</div>
<div class="land-index02">
<br>
<br>
<img src="<?php bloginfo( 'template_url' ); ?>/images/land/common/uchinada.jpg" alt="内灘" width="640" height="40">
</div>
<?php if (have_posts()) : query_posts('cat=9'); ?>
<div class="land-common">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
<?php include (TEMPLATEPATH . '/loop-land.php'); ?>
</table>
</div>
<?php endif; ?>
 


</div>




</div>




<?php elseif ( in_category('3') ) : ?>

     <dl>
     
     <?php
        $category_description = category_description();
        if ( ! empty( $category_description ) )
        echo '<div class="archive-meta">' . $category_description . '</div>';
    
        /* Run the loop for the category page to output the posts.
        * If you want to overload this in a child theme then include a file
        * called loop-category.php and that will be used instead.
        */
        get_template_part( 'loop', 'category' );
        ?>
         <br class="clear">
    </dl>


<?php else : ?>

<?php endif; ?>
            
            

<?php /* Display navigation to next/previous pages when applicable */ ?>
<?php if (  $wp_query->max_num_pages > 1 ) : ?>
				<div id="nav-below" class="navigation">
					
					
                    
                    <div class="nav-next"><?php previous_posts_link('<img src="http://nakaju.com/wp-content/themes/nakamura/images/common/nav/back.jpg" />') ?></div>
                    <div class="nav-previous"><?php next_posts_link('<img src="http://nakaju.com/wp-content/themes/nakamura/images/common/nav/next.jpg" />') ?></div>
                    <br class="clear">
				</div><!-- #nav-below -->
<?php endif; ?>

			</div><!-- #content -->
		</div><!-- #container -->

</div>

</div>
<?php get_sidebar(); ?>

<div class="clear"></div>
</div>
<p class="PageUp2"><a href="#top"><img src="<?php bloginfo( 'template_url' ); ?>/images/common/parts/up_off.jpg" alt="ページトップ" width="88" height="11"></a></p>
</div>
</div>




		


<?php get_footer(); ?>
