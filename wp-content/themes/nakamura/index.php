<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(index); ?>


<div id="Contents">

<div class="IndexMain">
<div class="IndexBanner">
<div class="Figure">
<a href="/gallery" target="_blank"><img src="<?php bloginfo( 'template_url' ); ?>/images/index/img01.jpg" alt="DesignGallery"></a>
</div>
</div>

<div class="IndexArea01">
<div class="Article clearfix">
<div class="InfoSectionL">
<h1><img src="<?php bloginfo( 'template_url' ); ?>/images/index/title01.jpg" alt="イベント情報＆最新情報" width="507" height="84"></h1>
<p class="list"><a href="/news/"><img src="<?php bloginfo( 'template_url' ); ?>/images/common/parts/list_off.jpg" width="43" height="12" alt="一覧"></a></p>
<ul>



<?php $posts=get_posts('numberposts=6&category=3,6'); ?>
<?php if ( $posts ) : foreach($posts as $post) : setup_postdata($post); ?>

<li>
<span class="date"><?php the_time("Y年m月d日"); ?></span>
<a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %s', 'twentyten' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"><?php the_title(); ?></a>
</li>


<?php endforeach; endif; ?>  



</ul>

<img src="<?php bloginfo( 'template_url' ); ?>/images/index/img02.jpg" width="509" height="290">
</div>
<div class="InfoSectionR clearfix">
<h1><img src="<?php bloginfo( 'template_url' ); ?>/images/index/title02.jpg" alt="施工事例集" width="91" height="593">
<p class="list"><a href="/works/"><img src="<?php bloginfo( 'template_url' ); ?>/images/common/parts/list_off.jpg" width="43" height="12" alt="一覧"></a></p></h1>
<div class="right">
<img src="<?php bloginfo( 'template_url' ); ?>/images/index/img03.jpg" width="312" height="120" class="MB5px">
<div class="IndexList">
<ul>

<?php $posts=get_posts('numberposts=4&category=5'); ?>
<?php if ( $posts ) : foreach($posts as $post) : setup_postdata($post); ?>
<li class="clearfix">

<?php

$imagefield = get_imagefield('works-photo-s');
$attachment = get_attachment_object($imagefield['id']);

echo '<image src="' . $attachment['url'] . '" width="92" />';

 ?>


<div class="text">
<span class="date"><?php the_time("Y年m月d日"); ?></span>
<?php the_title(); ?>
<br>
<?php echo c2c_get_custom('works-01'); ?><span class="link"><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %s', 'twentyten' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark">続きを読む</a></span>
</div>
</li>
<?php endforeach; endif; ?>  
</ul>
</div>
</div>
</div>
</div>
</div>
<div class="IndexArea01">
<div class="Article clearfix">
<div class="VoiceSectionL">
<h1><img src="<?php bloginfo( 'template_url' ); ?>/images/index/img04.jpg" alt="お届けしたいお客様の声" width="622" height="81"></h1>
<h2 class="clearfix">
<span class="left"><img src="<?php bloginfo( 'template_url' ); ?>/images/index/img05.jpg" alt="住み心地は？スタッフの対応は？施主様の感想を大公開！" width="370" height="14"></span>
<span class="right"><a href="/voice/"><img src="<?php bloginfo( 'template_url' ); ?>/images/common/parts/list_off.jpg" width="43" height="12" alt="一覧"></a></span>
</h2>
<ul>
<li><img src="<?php bloginfo( 'template_url' ); ?>/images/index/img06.jpg" width="125" height="99"></li>
<li><img src="<?php bloginfo( 'template_url' ); ?>/images/index/img07.jpg" width="125" height="99"></li>
<li><img src="<?php bloginfo( 'template_url' ); ?>/images/index/img08.jpg" width="125" height="99"></li>
<li><img src="<?php bloginfo( 'template_url' ); ?>/images/index/img09.jpg" width="125" height="99"></li>
<div class="clear"></div>
</ul>
</div>
<div class="VoiceSectionR">
<div class="IndexList">

<ul>
<?php $posts=get_posts('numberposts=2&category=4'); ?>
<?php if ( $posts ) : foreach($posts as $post) : setup_postdata($post); ?>


 
<li class="clearfix">
<?php

$imagefield = get_imagefield('voice-photo01');
$attachment = get_attachment_object($imagefield['id']);

echo '<image src="' . $attachment['url'] . '" alt="' . $attachment['title'] . '" title="' . $attachment['content'] . '"  width="92" />';

 ?>
<!--
<img src="<?php bloginfo( 'template_url' ); ?>/images/dammy/dammy05.jpg">-->
<div class="text">
<span class="case">【<?php echo c2c_get_custom('voice-txt01'); ?>】</span>
<span class="voice"><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %s', 'twentyten' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"><?php the_title(); ?></a></span>
<?php echo c2c_get_custom('voice-txt02'); ?><span class="link"><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %s', 'twentyten' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark">続きを読む</a></span>
</div>
</li>

<?php endforeach; endif; ?>  


</ul>
</div>
</div>
</div>
</div>

<div class="IndexArea02">
<div class="Article clearfix">
<div class="BlogSectionL">
<h1><a href="http://nakamurajutaku.blog.fc2.com/" target="_blank"><img src="<?php bloginfo( 'template_url' ); ?>/images/index/img10.jpg" width="312" height="171"></a></h1>
<h2><a href="http://nakamurajutaku.blog.fc2.com/" target="_blank"><img src="<?php bloginfo( 'template_url' ); ?>/images/index/img11_off.jpg" width="312" height="57" alt="スタッフブログ"></a></h2>
<div class="IndexList">
<div style="overflow: hidden;" id="feed"></div>
</div>
</div>
<div class="BlogSectionR">
<a href="/modelhouse"><img src="<?php bloginfo( 'template_url' ); ?>/images/index/img12_off.jpg" width="610" height="454" alt="モデルハウスを見に行こう"></a>
</div>

</div>
</div>
<div class="IndexArea03">
<div class="Article clearfix"> <a href="/land/"><img src="<?php bloginfo( 'template_url' ); ?>/images/index/img13_off.jpg" width="313" height="139" alt="土地探しから始めたい。" class="left"></a>
<div class="Botton right">

<ul>
<li><a href="/concept/planning/index.html"><img src="http://nakaju.com/images/index/menu1_off.png" width="89" height="87"></a></li>
<li><a href="/concept/technology/index.html"><img src="http://nakaju.com/images/index/menu2_off.png" width="88" height="87"></a></li>
<li><a href="/concept/support/index.html"><img src="http://nakaju.com/images/index/menu3_off.png" width="92" height="87"></a></li>
</ul>
</div>


</div>
</div>

<p class="PageUp"><a href="#top"><img src="<?php bloginfo( 'template_url' ); ?>/images/common/parts/up_off.jpg"></a></p>
</div>
</div>





<?php get_footer(); ?>