<?php
/**
 * The loop that displays a single post.
 *
 * The loop displays the posts and the post content.  See
 * http://codex.wordpress.org/The_Loop to understand it and
 * http://codex.wordpress.org/Template_Tags to understand
 * the tags used in it.
 *
 * This can be overridden in child themes with loop-single.php.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.2
 */
?>


<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
<div id="land-page">

<div class="land-page">


<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<h3 class="entry-title"><?php the_title(); ?> <?php echo c2c_get_custom('voice-txt02'); ?> <?php echo c2c_get_custom('land-01'); ?></h3>
<p class="txt-left"><?php echo c2c_get_custom('land-02'); ?></p>
<p class="txt-rihgt"><?php $cats = get_the_category(); foreach($cats as $cat):if($cat->parent)echo $cat->cat_name . ' ';endforeach; ?></p>
<br class="clear">
    <div class="land-page02">               
        <div class="entry-content">
            <?php the_content(); ?>
            <br class="clear" />
        </div><!-- .entry-content -->
        
        
    
    </div>

</div>

</div>



</div><!-- #post-## -->

					<div class="entry-utility">
						
						<?php edit_post_link( __( 'Edit', 'twentyten' ), '<span class="edit-link">', '</span>' ); ?>
					</div><!-- .entry-utility -->
			

		
<?php endwhile; // end of the loop. ?>
