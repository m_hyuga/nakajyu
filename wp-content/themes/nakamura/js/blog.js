﻿// JavaScript Document
google.load("feeds", "1");

function initialize() {
  var feedurl = "http://nakamurajutaku.blog.fc2.com/?xml";
  var feed = new google.feeds.Feed(feedurl);
  feed.setNumEntries(2);
  feed.load(dispfeed);

  function dispfeed(result){
    if (!result.error){
      var container = document.getElementById("feed");
      var htmlstr = "";
  

      htmlstr += "<ul>";
      for (var i = 0; i < result.feed.entries.length; i++) {
        var entry = result.feed.entries[i];

        htmlstr += "<li>"
		var strdate = createDateString(entry.publishedDate);
        htmlstr += '<span class="date">' + strdate + '</span>';
   
        htmlstr += entry.title + '<br>';
		htmlstr += '<a href="' + entry.link + '" target="_blank">' + '続きを読む' + '</a>';
        htmlstr += "</li>"
       
      }
      htmlstr += "</ul>";

       container.innerHTML = htmlstr;
    }else{
       alert(result.error.code + ":" + result.error.message);
    }
  }
}

function createDateString(publishedDate){
  var pdate = new Date(publishedDate);

  var pday = pdate.getDate();
  var pmonth = pdate.getMonth() + 1;
  var pyear = pdate.getFullYear();
  var phour = pdate.getHours();
  var pminute = pdate.getMinutes();
  var psecond = pdate.getSeconds();
  var strdate = pyear + "." + pmonth + "." + pday  ;

  return strdate;
}

google.setOnLoadCallback(initialize);


/*

google.load("feeds", "1");
var entryArray = new Array();
var entryNum = 0;
function initialize() {
feedAdd("http://pipes.yahoo.com/pipes/pipe.run?URL=http%3A%2F%2Frssblog.ameba.jp%2Fpurplehaze-staff%2Frss20.xml&_id=DrFZRK663RGaE1sE1pzWFw&_render=rss", 1);
}
function feedAdd(rssUrl, boolNum) {
var feed = new google.feeds.Feed(rssUrl);
feed.setNumEntries(5);
feed.load(function(result) {
if (!result.error) {
for (var i = 0; i < result.feed.entries.length; i++) {
entryArray[entryNum] = result.feed.entries[i];
var date = new Date(result.feed.entries[i].publishedDate);
entryArray[entryNum].sortDate = ( date.getFullYear()*1000000 ) + ( (date.getMonth() + 1)*3600*32 ) + ( date.getDate()*3600 ) + ( date.getHours()*60 ) + date.getMinutes();
entryArray[entryNum].blogName = result.feed.title;
entryNum+=1;
}
}
if(boolNum==1){
feedOutput("feed", 100)
}
});
}
function feedOutput(feedId, listNum){
var useFeed = "";
var useDate = "";
var container = document.getElementById(feedId);
entryArray = asort(entryArray, "sortDate");
if(listNum==100){
listNum = entryNum;
}
for (var i = 0; i < listNum; i++) {
var entry = entryArray[i];
var date = new Date(entry.publishedDate);
var yy = date.getYear();
if (yy < 2000) yy += 1900;
var m = date.getMonth() + 1;
if (m < 10) {m = "0" + m;}
var d = date.getDate();
if (d < 10) {d = "0" + d;}
var h = date.getHours();
if (h < 10) {h = "0" + h;}
var mn = date.getMinutes();
if (mn < 10) {mn = "0" + mn;}
useDate = yy + "年" + m + "月" + d + "日";
useFeed +='<li>'  +  '<span class="date">'  + yy + "." + m + "." + d  + ' ' + '</span><p style="overflow:hidden;"><a href="' + entry.link + '" target="_blank" title="' + entry.title + '【' + entry.blogName + '】">' + entry.title + '</a> <img src="http://b.hatena.ne.jp/entry/image/large/' + entry.link + '"></p>' + '</li>';
}
container.innerHTML = '<div>' + useFeed + '<div>';
}
function asort(myArray, key){
return myArray.sort ( function (b1, b2) { return b1[key] > b2[key] ? -1 : 1; } );
}
google.setOnLoadCallback(initialize);
*/