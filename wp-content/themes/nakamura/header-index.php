<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="Keywords" content="住宅会社,中村住宅開発,石川県,金沢市,住宅,モデル住宅,分譲住宅,注文住宅,ハウスメーカー,住宅メーカー">
<meta name="Description" content="中村住宅開発は石川県金沢市の住宅会社です。お客様1人1人の理想の暮らしを実現するため、中村住宅開発は「デザイン」で答えます。自由度の高い設計と、安心安全なサポートで「家族の未来」をデザインします。">

<title><?php
	/*
	 * Print the <title> tag based on what is being viewed.
	 */
	global $page, $paged;

	wp_title( '|', true, 'right' );

	// Add the blog name.
	bloginfo( 'name' );


	?>┃石川県金沢市の注文住宅</title>
<link href="<?php bloginfo( 'template_url' ); ?>/css/style.css" rel="stylesheet" type="text/css" media="all">
<link href="<?php bloginfo( 'template_url' ); ?>/css/common.css" rel="stylesheet" type="text/css" media="all">

<?php  if(is_home()) : // ホームなら  ?>
<link href="<?php bloginfo( 'template_url' ); ?>/css/index.css" rel="stylesheet" type="text/css" media="all">
<link href="<?php bloginfo( 'template_url' ); ?>/css/image_navigation.css" rel="stylesheet" type="text/css" media="all">
<?php else : ?>
<link href="<?php bloginfo( 'template_url' ); ?>/css/page02.css" rel="stylesheet" type="text/css" media="all">
<?php endif;	// is_home() ?>
<script src="<?php bloginfo( 'template_url' ); ?>/js/smartRollover.js"></script>
<!--[if IE 6]>
<script src="js/DD_belatedPNG.js"></script>
<script>
DD_belatedPNG.fix('img, .png_bg');
</script>
<![endif]-->
<script type="text/javascript" src="<?php bloginfo( 'template_url' ); ?>/js/slider-pro-master/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script src="<?php bloginfo( 'template_url' ); ?>/js/blog.js"></script>
<script src="<?php bloginfo( 'template_url' ); ?>/js/slider-pro-master/jquery.sliderPro.js"></script>
<link href="<?php bloginfo( 'template_url' ); ?>/js/slider-pro-master/slider-pro.css" rel="stylesheet" type="text/css" media="all">
<script type="text/javascript">
	$( document ).ready(function( $ ) {
		$( '#main_slide' ).sliderPro({
			width: 770,
			height: 460,
			slideDistance:0,
			fade:true,
			centerImage:false,
			fadeOutPreviousSlide:true,
			responsive:false,
			orientation: 'vertical',
			loop: false,
			arrows: true,
			buttons: false,
			thumbnailsPosition: 'right',
			thumbnailPointer: true,
			thumbnailWidth: 190,
			thumbnailHeight:140
		});
	});
</script>
</head>
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php
	/* We add some JavaScript to pages with the comment form
	 * to support sites with threaded comments (when in use).
	 */
	if ( is_singular() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );

	/* Always have wp_head() just before the closing </head>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to add elements to <head> such
	 * as styles, scripts, and meta tags.
	 */
	wp_head();
?>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-9506923-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>

<body <?php body_class(); ?>>
<div id="Wrapper"><a name="top"></a>

<!--ヘッダ-->
<div id="Header_index">
<div id="HeaderInner">
<div class="Header clearfix">
<h1><a href="/"><img src="<?php bloginfo( 'template_url' ); ?>/images/common/header/logo.jpg" alt="石川県金沢市の住宅会社 中村住宅開発" width="242" height="85"></a></h1>
<div class="tel">
<a href="/form/"><img src="<?php bloginfo( 'template_url' ); ?>/images/common/header/img02.jpg" alt="お問合せ・資料請求 076-221-7666" width="332" height="49" ></a><img src="<?php bloginfo( 'template_url' ); ?>/images/common/header/img03.jpg" alt="メンテナンスに関するお問合せはこちらから 076-260-5002" width="332" height="49" >
<div class="nav">
<ul>
<li><a href="/other/recruit.html">採用情報</a></li>
<li><a href="/other/sitemap.html">サイトマップ</a></li>
<li><a href="/other/privacypolicy.html">プライバシーポリシー</a></li>
</ul>
</div>
</div>
</div>
</div>
</div>

<div id="image-navigation">
<div class="navi clearfix">
<ul>
<li><a href="/"><img src="<?php bloginfo( 'template_url' ); ?>/images/common/nav/nav01_off.jpg" alt="HOME" width="51" height="46"></a></li>
<li><a href="/gallery" target="_blank"><img src="<?php bloginfo( 'template_url' ); ?>/images/common/nav/nav02_off.jpg" alt="デザイン研究所" width="115" height="46" class="ML4"></a></li>
<li><a href="/works"><img src="<?php bloginfo( 'template_url' ); ?>/images/common/nav/nav03_off.jpg" alt="施工事例集" width="76" height="46" class="ML4"></a></li>
<li><a href="/voice"><img src="<?php bloginfo( 'template_url' ); ?>/images/common/nav/nav04_off.jpg" alt="お客様の声" width="114" height="46" class="ML4"></a></li>
<li><a href="/news"><img src="<?php bloginfo( 'template_url' ); ?>/images/common/nav/nav05_off.jpg" alt="イベント＆新着情報" width="113" height="46" class="ML4"></a></li>
<li><a href="/modelhouse"><img src="<?php bloginfo( 'template_url' ); ?>/images/common/nav/nav06_off.jpg" alt="モデルハウスを見に行こう" width="140" height="46" class="ML4"></a></li>
<li><a href="/land"><img src="<?php bloginfo( 'template_url' ); ?>/images/common/nav/nav07_off.jpg" alt="土地情報" width="124" height="46" class="ML4"></a></li>
<li><a href="/concept"><img src="<?php bloginfo( 'template_url' ); ?>/images/common/nav/nav08_off.jpg" alt="中村住宅開発の家づくり" width="132" height="46" class="ML4"></a></li>
<li><a href="/other/profile.html"><img src="<?php bloginfo( 'template_url' ); ?>/images/common/nav/nav09_off.jpg" alt="会社案内" width="63" height="46" class="ML4"></a></li>
</ul>
</div>

<div class="MainArea">

	<div class="slide_area">
	<div id="main_slide" class="slider-pro">
		<div class="sp-slides">
			<div class="sp-slide"><a href="/modelhouse/"><img class="sp-image" src="<?php bloginfo( 'template_url' ); ?>/images/index/main/slide-01.jpg" align="モデルハウス"/></a></div>
			<div class="sp-slide"><a href="/gallery/" target="_blank"><img class="sp-image" src="<?php bloginfo( 'template_url' ); ?>/images/index/main/main02.jpg" align="デザイン研究室"/></a></div>
			<div class="sp-slide"><a href="/category/works"><img class="sp-image" src="<?php bloginfo( 'template_url' ); ?>/images/index/main/main03.jpg" align="施工事例集"/></a></div>
			<div class="sp-slide"><a href="/category/voice"><img class="sp-image" src="<?php bloginfo( 'template_url' ); ?>/images/index/main/main04.jpg" align="お客様の声"/></a></div>
			<div class="sp-slide"><a href="/category/news"><img class="sp-image" src="<?php bloginfo( 'template_url' ); ?>/images/index/main/main05.jpg" align="イベント＆新着情報"/></a></div>
			<div class="sp-slide"><a href="/category/land"><img class="sp-image" src="<?php bloginfo( 'template_url' ); ?>/images/index/main/main06.jpg" align="土地情報"/></a></div>
			<div class="sp-slide"><a href="/concept/"><img class="sp-image" src="<?php bloginfo( 'template_url' ); ?>/images/index/main/main08.jpg" align="中村住宅開発の家づくり"/></a></div>
		</div>
		<div class="sp-thumbnails">
			<div class="sp-thumbnail"><img class="sp-thumbnail-image" src="<?php bloginfo( 'template_url' ); ?>/images/index/main/thumb-01.jpg" align="モデルハウス公開中"/></div>
			<div class="sp-thumbnail"><img class="sp-thumbnail-image" src="<?php bloginfo( 'template_url' ); ?>/images/index/main/thumb-03.jpg" align="デザイン研究室"/></div>
			<div class="sp-thumbnail"><img class="sp-thumbnail-image" src="<?php bloginfo( 'template_url' ); ?>/images/index/main/thumb-04.jpg" align="施工事例集"/></div>
			<div class="sp-thumbnail"><img class="sp-thumbnail-image" src="<?php bloginfo( 'template_url' ); ?>/images/index/main/thumb-05.jpg" align="お客様の声"/></div>
			<div class="sp-thumbnail"><img class="sp-thumbnail-image" src="<?php bloginfo( 'template_url' ); ?>/images/index/main/thumb-06.jpg" align="イベント＆新着情報"/></div>
			<div class="sp-thumbnail"><img class="sp-thumbnail-image" src="<?php bloginfo( 'template_url' ); ?>/images/index/main/thumb-07.jpg" align="土地情報"/></div>
			<div class="sp-thumbnail"><img class="sp-thumbnail-image" src="<?php bloginfo( 'template_url' ); ?>/images/index/main/thumb-08.jpg" align="中村住宅開発の家づくり"/></div>
		</div>
    </div>
		</div>

</div>
</div>