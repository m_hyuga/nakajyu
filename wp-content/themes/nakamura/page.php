<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

<div id="MainArea">
<div id="Crumbs"><a href="<?php bloginfo('url'); ?>">Home</a> &gt; 
	
	<?php the_title(); ?></div>

<div class="Figure"><img src="../images/contact/main.jpg" width="960" height="300" alt="お問い合わせ・カタログ請求"></div>
</div>


<div id="Contents">
<div class="Article">
<div class="PageMain">

<div class="SectionL"> 
		<div id="container">
			<div id="content" role="main">

			<?php
			/* Run the loop to output the page.
			 * If you want to overload this in a child theme then include a file
			 * called loop-page.php and that will be used instead.
			 */
			get_template_part( 'loop', 'page' );
			?>

			</div><!-- #content -->
		</div><!-- #container -->


</div>

<div class="SectionR">

<div class="SideNav"><img src="../images/contact/nav.jpg" width="270" height="68" alt="採用情報">
 
</div>


<a href="../works"><img src="../images/common/side/ban01.jpg" alt="施工事例集" width="270" height="95" class="MB5"></a>
<a href="../voice"><img src="../images/common/side/ban02.jpg" alt="お客様の声" width="270" height="95" class="MB5"></a>
<a href="../modelhouse"><img src="../images/common/side/ban03.jpg" alt="モデルハウス" width="270" height="95" class="MB5"></a>
<a href="http://nakamurajutaku.blog.fc2.com/" target="_blank"><img src="../images/common/side/ban04.jpg" alt="スタッフブログ" width="270" height="95" class="MB5"></a>
<a href="../land"><img src="../images/common/side/ban05.jpg" alt="土地探し" width="270" height="95" class="MB5"></a>
<a href="../concept"><img src="../images/common/side/ban06.jpg" alt="コンセプト" width="270" height="95"></a>
</div>

<div class="clear"></div>
</div>
<p class="PageUp2"><a href="#top"><img src="../images/common/parts/up_off.jpg" alt="ページトップ" width="88" height="11"></a></p>
</div>
</div>
<?php get_footer(); ?>
