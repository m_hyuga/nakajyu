<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>



<?php #ループ開始 ?>

<?php if ( in_category('3') ) : ?>

<?php include (TEMPLATEPATH . '/single-news.php'); ?>


<?php elseif ( in_category('4') ) : ?>

<?php include (TEMPLATEPATH . '/single-voice.php'); ?>


<?php elseif ( in_category('5') ) : ?>

<?php include (TEMPLATEPATH . '/single-works.php'); ?>


<?php elseif ( in_category('6') ) : ?>

<?php include (TEMPLATEPATH . '/single-land.php'); ?>



<?php else : ?>

<?php endif; ?>