<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

<div id="wrap">
<h2>土地情報</h2>

<section class="news-contents">

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>


<div class="land-con">
<h3 class="entry-title"><?php the_title(); ?> <?php echo c2c_get_custom('voice-txt02'); ?> <?php echo c2c_get_custom('land-01'); ?></h3>
<p class="txt-left"><?php echo c2c_get_custom('land-02'); ?></p>

<?php the_content(); ?>

</div><!-- #post-## -->
</br>
</br>
<img src="<?php bloginfo( 'template_url' ); ?>/images/land/common/title_form01.jpg" width="100%">
<p>
HPには掲載されていない土地情報の希望がございましたら下記からリクエストしてください。
当社と提携している不動産会社のネットワークからお客様のご希望に沿う資料をセレクトしてお送りさせていただきます。
</p>
<?php echo do_shortcode('[contact-form-7 id="3612" title="土地情報リクエストフォーム"]'); ?>

<?php endwhile; // end of the loop. ?>

</section>
<section class="news-bottom">
<p>詳細につきましては、お気軽にお問い合わせください。</p>
	<p><a href="tel:076-221-7666"><img src="<?php bloginfo( 'template_url' ); ?>/images/common/tell.jpg" alt="076-221-7666" width="45%"></a></p>
	</section>
	<section class="form-bottom">
<p>メールフォームからのお問い合わせはこちらから</p>
<a href="/sp-form"><img src="<?php bloginfo( 'template_url' ); ?>/images/common/form.jpg" alt="お問い合わせ" width="45%"></a>
	</section>

<section class="top"><a href="#"><img src="<?php bloginfo( 'template_url' ); ?>/images/common/top.jpg" alt="ページトップ"></a> </section>
<section class="answer"><img src="<?php bloginfo( 'template_url' ); ?>/images/common/answer-design.jpg"></section>

</div>

<?php get_footer(); ?>