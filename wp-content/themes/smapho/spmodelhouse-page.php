<?php
/**
 * Template Name: spmodelhouse-page
 *
 * A custom page template without sidebar.
 *
 * The "Template Name:" bit above allows this to be selectable
 * from a dropdown menu on the edit page screen.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header("modelhouse"); ?>
			<?php
			/* Run the loop to output the page.
			 * If you want to overload this in a child theme then include a file
			 * called loop-page.php and that will be used instead.
			 */
			get_template_part( 'loop', 'page' );
			?>
<hr>
<div class="back">
<a href="javascript:history.back();"><img src="<?php bloginfo( 'url' ); ?>/sp/images/common/back.jpg" alt="戻る" width="45%"></a>
</div>
<section class="top">
 <a href="#"><img src="<?php bloginfo( 'url' ); ?>/sp/images/common/top.jpg" alt="ページトップ"></a> </section>
</div>
</div>
<?php get_footer(); ?>
