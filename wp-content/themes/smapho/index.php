<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(index); ?>


<div id="wrap">
<section>
<div id="mainImages" class="mainImageInit">
    <ul>
        <li id="mainImage0"><a href="news/"><img src="<?php bloginfo( 'template_url' ); ?>/images/index/img01.jpg" alt="" width="100%"></a></li>
        <li id="mainImage1"><a href="works/"><img src="<?php bloginfo( 'template_url' ); ?>/images/index/img11.jpg" alt="" width="100%"></a></li>
      	<li id="mainImage2"><a href="voice/"><img src="<?php bloginfo( 'template_url' ); ?>/images/index/img12.jpg" alt="" width="100%"></a></li>
        <li id="mainImage3"><a href="land/"><img src="<?php bloginfo( 'template_url' ); ?>/images/index/img13.jpg" alt="" width="100%"></a></li>
  		<li id="mainImage4"><a href="sp/modelhouse/"><img src="<?php bloginfo( 'template_url' ); ?>/images/index/img07.jpg" alt="" width="100%"></a></li>
 </ul>
</div>
<img src="<?php bloginfo( 'template_url' ); ?>/images/index/txt.jpg" alt="" width="100%">

</section>
<nav>
  <ul class="clearfix">
    <li><a href="news/"><img src="<?php bloginfo( 'template_url' ); ?>/images/index/img02.jpg"><p><span class="font32">イベント＆新着情報</span><br>
<span class="font22">家づくりの参考になる<br>イベント情報満載</span></p><br class="clear"></a>
</li>
    <li><a href="sp/modelhouse/index.html"><img src="<?php bloginfo( 'template_url' ); ?>/images/index/img03.jpg"><p><span class="font32">モデルハウスを見に行こう</span><br>
<span class="font22">住まいのコンシェルジュが<br>
お待ちしています</span></p><br class="clear"></a>
</li>
    <li><a href="works/"><img src="<?php bloginfo( 'template_url' ); ?>/images/index/img09.jpg"><p><span class="font32">施工事例集</span><br>
<span class="font22">お客様の住まいの実例<br>を紹介します</span></p><br class="clear"></a>
</li>
    <li><a href="voice/"><img src="<?php bloginfo( 'template_url' ); ?>/images/index/img10.jpg"><p><span class="font32">お客様の声</span><br>
<span class="font22">お届けしたい、お客さまの声</span></p><br class="clear"></a>
</li>

<li><a href="/category/land"><img src="<?php bloginfo( 'template_url' ); ?>/images/index/img08.jpg"><p><span class="font32">土地情報</span><br>
<span class="font22">住むなら何処がいいだろう<br>
私たちは土地選びから<br />
デザインします。</span></p><br class="clear"></a>
</li>
 <li><a href="sp/profile/index.html"><img src="<?php bloginfo( 'template_url' ); ?>/images/index/img05.jpg"><p><span class="font32">会社案内</span><br>
<span class="font22">中村住宅開発 <br>
会社概要</span></p><br class="clear"></a>
</li>
 <li><a href="sp-form/"><img src="<?php bloginfo( 'template_url' ); ?>/images/index/img04.jpg"><p><span class="font32">お問い合わせ・カタログ請求</span><br>
<span class="font22">住まいのご相談は<br>お問い合わせフォームより</span></p><br class="clear"></a>
</li>
</ul>
</nav>
<section class="answer"><img src="<?php bloginfo( 'template_url' ); ?>/images/common/answer-design.jpg"></section>

</div>





<?php get_footer(); ?>