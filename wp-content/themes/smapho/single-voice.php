<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

<div id="wrap">
<h2>お客様の声</h2>

<section class="news-contents">

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

<div class="news-con">
<h3><?php the_title(); ?></h3>

<?php the_content(); ?>
<div class="VoiceSectionR">
            <div class="VoiceSectionR-no01">
            <?php

            	$imagefield = get_imagefield('voice-photo01');
				$attachment = get_attachment_object($imagefield['id']);
				echo '<image src="' . $attachment['url'] . '" alt="' . $attachment['title'] . '" title="' . $attachment['content'] . '"  width="208" />';
            ?>
            </div>




             <?php
$imagefield = get_imagefield('voice-photo02');
$attachment = get_attachment_object($imagefield['id']);

if (empty($imagefield['id'])) {

echo '';

} else {
	 	echo '<div class="VoiceSectionR-no02">';
        echo '<image src="' . $attachment['url'] . '" alt="" width="320" />';
		echo '</div>';
		}
?>




             <?php
$imagefield = get_imagefield('voice-photo03');
$attachment = get_attachment_object($imagefield['id']);

if (empty($imagefield['id'])) {
       	echo '';

} else {
	 	echo '<div class="VoiceSectionR-no02">';
        echo '<image src="' . $attachment['url'] . '" alt="" width="320" />';
		echo '</div>';
}
?>

            <?php
$imagefield = get_imagefield('voice-photo04');
$attachment = get_attachment_object($imagefield['id']);

if (empty($imagefield['id'])) {
       	echo '';

} else {
	 	echo '<div class="VoiceSectionR-no02">';
        echo '<image src="' . $attachment['url'] . '" alt="" width="320" />';
		echo '</div>';
}
?>

            <?php
$imagefield = get_imagefield('voice-photo05');
$attachment = get_attachment_object($imagefield['id']);

if (empty($imagefield['id'])) {

       echo '';

} else {
	 	echo '<div class="VoiceSectionR-no02">';
        echo '<image src="' . $attachment['url'] . '" alt="" width="320" />';
		echo '</div>';

}
?>


            <?php
$imagefield = get_imagefield('voice-photo06');
$attachment = get_attachment_object($imagefield['id']);

if (empty($imagefield['id'])) {

       echo '';

} else {
	 	echo '<div class="VoiceSectionR-no02">';
        echo '<image src="' . $attachment['url'] . '" alt="" width="320" />';
		echo '</div>';

}
?>


            <?php
$imagefield = get_imagefield('voice-photo07');
$attachment = get_attachment_object($imagefield['id']);

if (empty($imagefield['id'])) {

       echo '';

} else {
	 echo '<div class="VoiceSectionR-no02">';
        echo '<image src="' . $attachment['url'] . '" alt="" width="320" />';
		echo '</div>';

}
?>



</div>

<br class="clear">



<?php
$imagefield = get_imagefield('voice-photo08');
$attachment = get_attachment_object($imagefield['id']);

if (empty($imagefield['id'])) {

       echo '';

} else {
	 echo '<div class="VoiceSection-b">';
        echo '<image src="' . $attachment['url'] . '" alt="" width="670" />';
		echo '</div>';

}
?>

</div><!-- #post-## -->
<img src="<?php bloginfo( 'template_url' ); ?>/images/voice/common/title_form01.jpg" width="100%">
<p>
HPには掲載されていないお客様様の声の希望がございましたら下記からリクエストしてください。
当社で建てさせていただいた数多くの施主様のご感想から、お客様のご希望に沿う資料をセレクトしてお送りさせていただきます。
</p>
<?php echo do_shortcode('[contact-form-7 id="3613" title="お客様の声リクエストフォーム"]'); ?>

<?php endwhile; // end of the loop. ?>

</section>
<section class="news-bottom">
<p>詳細につきましては、お気軽にお問い合わせください。</p>
	<p><a href="tel:076-221-7666"><img src="<?php bloginfo( 'template_url' ); ?>/images/common/tell.jpg" alt="076-221-7666" width="45%"></a></p>
	</section>
	<section class="form-bottom">
<p>メールフォームからのお問い合わせはこちらから</p>
<a href="/sp-form"><img src="<?php bloginfo( 'template_url' ); ?>/images/common/form.jpg" alt="お問い合わせ" width="45%"></a>
	</section>

<section class="top"><a href="#"><img src="<?php bloginfo( 'template_url' ); ?>/images/common/top.jpg" alt="ページトップ"></a> </section>
<section class="answer"><img src="<?php bloginfo( 'template_url' ); ?>/images/common/answer-design.jpg"></section>

</div>

<?php get_footer(); ?>