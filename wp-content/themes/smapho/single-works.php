<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

<div id="wrap">
<h2>施工事例集</h2>

<section class="news-contents">

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

<div class="news-con">
<h3><?php the_title(); ?></h3>

<?php the_content(); ?>

</div><!-- #post-## -->



<!-- 一列目 -->
<div class="Main-Photo">
<div class="Main-PhotoL">

<?php
$imagefield = get_imagefield('works-photo01');
$attachment = get_attachment_object($imagefield['id']);

if (empty($imagefield['id'])) {
echo '';
} else {
        echo '<image src="' . $attachment['url'] . '" alt="" width="320" />';
		}
?>
<?php echo c2c_get_custom('works-title01','<h4>','</h4>'); ?>
<?php echo c2c_get_custom('works-txt01','<p>','</p>'); ?>
</div>

<div class="Main-PhotoR">
<?php
$imagefield = get_imagefield('works-photo02');
$attachment = get_attachment_object($imagefield['id']);

if (empty($imagefield['id'])) {
echo '';
} else {
        echo '<image src="' . $attachment['url'] . '" alt="" width="320" />';
		}
?>
<?php echo c2c_get_custom('works-title02','<h4>','</h4>'); ?>
<?php echo c2c_get_custom('works-txt02','<p>','</p>'); ?>
</div>
  <br class="clear">
</div>
<!-- 一列目ここまで -->


<!-- 二列目 -->
<div class="Main-Photo">
<div class="Main-PhotoL">

<?php
$imagefield = get_imagefield('works-photo03');
$attachment = get_attachment_object($imagefield['id']);

if (empty($imagefield['id'])) {
echo '';
} else {
        echo '<image src="' . $attachment['url'] . '" alt="" width="320" />';
		}
?>
<?php echo c2c_get_custom('works-title03','<h4>','</h4>'); ?>
<?php echo c2c_get_custom('works-txt03','<p>','</p>'); ?>
</div>

<div class="Main-PhotoR">
<?php
$imagefield = get_imagefield('works-photo04');
$attachment = get_attachment_object($imagefield['id']);

if (empty($imagefield['id'])) {
echo '';
} else {
        echo '<image src="' . $attachment['url'] . '" alt="" width="320" />';
		}
?>
<?php echo c2c_get_custom('works-title04','<h4>','</h4>'); ?>
<?php echo c2c_get_custom('works-txt04','<p>','</p>'); ?>
</div>
  <br class="clear">
</div>
<!-- 二列目ここまで -->


<!-- 三列目 -->
<div class="Main-Photo">
<div class="Main-PhotoL">

<?php
$imagefield = get_imagefield('works-photo05');
$attachment = get_attachment_object($imagefield['id']);

if (empty($imagefield['id'])) {
echo '';
} else {
        echo '<image src="' . $attachment['url'] . '" alt="" width="320" />';
		}
?>
<?php echo c2c_get_custom('works-title05','<h4>','</h4>'); ?>
<?php echo c2c_get_custom('works-txt05','<p>','</p>'); ?>
</div>

<div class="Main-PhotoR">
<?php
$imagefield = get_imagefield('works-photo06');
$attachment = get_attachment_object($imagefield['id']);

if (empty($imagefield['id'])) {
echo '';
} else {
        echo '<image src="' . $attachment['url'] . '" alt="" width="320" />';
		}
?>
<?php echo c2c_get_custom('works-title06','<h4>','</h4>'); ?>
<?php echo c2c_get_custom('works-txt06','<p>','</p>'); ?>
</div>
  <br class="clear">
</div>
<!-- 三列目ここまで -->
<img src="<?php bloginfo( 'template_url' ); ?>/images/works/common/title_form01.jpg" width="100%">
<p>
HPには掲載されていない事例の希望がございましたら下記からリクエストしてください。
当社が過去に手掛けた数多くの事例から、お客様のご希望に沿う事例紹介資料をセレクトしてお送りさせていただきます。
</p>

<?php echo do_shortcode('[contact-form-7 id="3614" title="施工事例リクエストフォーム"]'); ?>

<?php endwhile; // end of the loop. ?>

</section>
<section class="news-bottom">
<p>詳細につきましては、お気軽にお問い合わせください。</p>
	<p><a href="tel:076-221-7666"><img src="<?php bloginfo( 'template_url' ); ?>/images/common/tell.jpg" alt="076-221-7666" width="45%"></a></p>
	</section>
	<section class="form-bottom">
<p>メールフォームからのお問い合わせはこちらから</p>
<a href="/sp-form"><img src="<?php bloginfo( 'template_url' ); ?>/images/common/form.jpg" alt="お問い合わせ" width="45%"></a>
	</section>

<section class="top"><a href="#"><img src="<?php bloginfo( 'template_url' ); ?>/images/common/top.jpg" alt="ページトップ"></a> </section>
<section class="answer"><img src="<?php bloginfo( 'template_url' ); ?>/images/common/answer-design.jpg"></section>

</div>

<?php get_footer(); ?>