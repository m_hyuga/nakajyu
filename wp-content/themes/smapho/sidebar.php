<?php
/**
 * The Sidebar containing the primary and secondary widget areas.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?>


<div class="SectionR">

<div class="SideNav">
  <img src="<?php bloginfo( 'template_url' ); ?>/images/common/side/mtop.jpg" alt="" width="270" height="18" class="imgTop">
   <?php include (TEMPLATEPATH . '/title-sidebar.php'); ?>
  <img src="<?php bloginfo( 'template_url' ); ?>/images/common/side/mbottom.jpg" alt="" width="270" height="10">
</div>
<a href="/works/"><img src="<?php bloginfo( 'template_url' ); ?>/images/common/side/ban01.jpg" alt="施工事例集" width="270" height="95" class="MB5"></a>
<a href="/voice/"><img src="<?php bloginfo( 'template_url' ); ?>/images/common/side/ban02.jpg" alt="お客様の声" width="270" height="95" class="MB5"></a>
<a href="/modelhouse/"><img src="<?php bloginfo( 'template_url' ); ?>/images/common/side/ban03.jpg" alt="モデルハウス" width="270" height="95" class="MB5"></a>
<a href="http://nakamurajutaku.blog.fc2.com/" target="_blank"><img src="<?php bloginfo( 'template_url' ); ?>/images/common/side/ban04.jpg" alt="スタッフブログ" width="270" height="95" class="MB5"></a>
<a href="/land/"><img src="<?php bloginfo( 'template_url' ); ?>/images/common/side/ban05.jpg" alt="土地探し" width="270" height="95" class="MB5"></a>
<a href="/concept/"><img src="<?php bloginfo( 'template_url' ); ?>/images/common/side/ban06.jpg" alt="コンセプト" width="270" height="95"></a>
</div>




		
