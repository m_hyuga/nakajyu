<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta name="viewport" content="width=device-width, user-scalable=yes, initial-scale=1, maximum-scale=1"/>

<title><?php
	/*
	 * Print the <title> tag based on what is being viewed.
	 */
	global $page, $paged;

	wp_title( '|', true, 'right' );

	// Add the blog name.
	bloginfo( 'name' );


	?>┃石川県金沢市の注文住宅</title>


<link rel="stylesheet" href="<?php bloginfo( 'template_url' ); ?>/css/reset.css">
<link rel="stylesheet" href="<?php bloginfo( 'template_url' ); ?>/css/common.css">

<script type="text/javascript" src="http://www.google.com/jsapi"></script>
<script type="text/javascript">
    //<![CDATA[
google.load("jquery", "1");    //]]>
</script>
<script src="<?php bloginfo( 'template_url' ); ?>/js/jquery.flickslide.js" type="application/javascript" charset="UTF-8"></script>
<link rel="stylesheet" type="text/css" href="<?php bloginfo( 'template_url' ); ?>/css/flickslide.css" />

<script type="application/javascript">
<!--//
$(function(){
    $('#mainImages ul li').flickSlide({target:'#mainImages>ul', duration:4000});
});
//-->
</script>
<!--[if IE 6]>
<script src="js/DD_belatedPNG.js"></script>
<script>
DD_belatedPNG.fix('img, .png_bg');
</script>
<![endif]-->
</head>



<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php
	/* We add some JavaScript to pages with the comment form
	 * to support sites with threaded comments (when in use).
	 */
	if ( is_singular() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );

	/* Always have wp_head() just before the closing </head>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to add elements to <head> such
	 * as styles, scripts, and meta tags.
	 */
	wp_head();
?>
<? wp_enqueue_script('jquery')  ?>


</head>
<body>

<header id="top">
  <h1><a href="<?php echo home_url( '/' ); ?>"><img src="<?php bloginfo( 'template_url' ); ?>/images/common/logo.jpg" alt="中村住宅開発"></a></h1>
  
<div class="tell">
<a href="tel:076-221-7666"><img src="<?php bloginfo( 'template_url' ); ?>/images/common/tell.jpg" alt="076-221-7666"></a><br>
<a href="tel:076-221-7666"><img src="<?php bloginfo( 'template_url' ); ?>/images/common/tell03.jpg" alt="076-221-7666"></a>
</div>
  
  
  <br class="clear">

</header>
