<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

<div id="wrap">
<h2>イベント＆新着情報</h2>

<section class="news-contents">

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

<div class="news-con">
<h3><?php the_title(); ?></h3>

<?php the_content(); ?>

</div><!-- #post-## -->

<?php endwhile; // end of the loop. ?>

</section>
<section class="news-bottom">
<p>詳細につきましては、お気軽にお問い合わせください。</p>
	<p><a href="tel:076-221-7666"><img src="<?php bloginfo( 'template_url' ); ?>/images/common/tell.jpg" alt="076-221-7666" width="45%"></a></p>
	</section>
	<section class="form-bottom">
<p>メールフォームからのお問い合わせはこちらから</p>
<a href="/sp-form"><img src="<?php bloginfo( 'template_url' ); ?>/images/common/form.jpg" alt="お問い合わせ" width="45%"></a>
	</section>

<section class="top"><a href="#"><img src="<?php bloginfo( 'template_url' ); ?>/images/common/top.jpg" alt="ページトップ"></a> </section>
<section class="answer"><img src="<?php bloginfo( 'template_url' ); ?>/images/common/answer-design.jpg"></section>

</div>

<?php get_footer(); ?>