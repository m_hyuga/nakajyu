<?php
/**
 * The template for displaying Category Archive pages.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

<div id="wrap">



            
<?php #ループ開始 ?>

<?php if ( in_category('3') ) : ?>
<h2>イベント＆新着情報</h2>
<section class="main-photo">
<img src="<?php bloginfo( 'template_url' ); ?>/images/news/photo01.jpg" alt="" width="100%"></section>
<section class="news">


<?php elseif ( in_category('4') ) : ?>
<h2>お客様の声</h2>
<section class="main-photo">
<img src="<?php bloginfo( 'template_url' ); ?>/images/voice/photo01.jpg" alt="" width="100%"></section>
<section class="news">


<?php elseif ( in_category('5') ) : ?>
<h2>施工事例集</h2>
<section class="main-photo">
<img src="<?php bloginfo( 'template_url' ); ?>/images/works/photo01.jpg" alt="" width="100%"></section>
<section class="news">


<?php elseif ( in_category('6') ) : ?>
<h2>土地情報</h2>
<section class="main-photo">
<img src="<?php bloginfo( 'template_url' ); ?>/images/land/photo01.jpg" alt="" width="100%"></section>
<section class="news">
	 
     
     <?php else : ?>


<?php endif; ?>
     <?php
        $category_description = category_description();
        if ( ! empty( $category_description ) )
        echo '<div class="archive-meta">' . $category_description . '</div>';
    
        /* Run the loop for the category page to output the posts.
        * If you want to overload this in a child theme then include a file
        * called loop-category.php and that will be used instead.
        */
        get_template_part( 'loop', 'category' );
        ?>







<div class="page">

<?php /* Display navigation to next/previous pages when applicable */ ?>
<?php if (  $wp_query->max_num_pages > 1 ) : ?>
	<div id="nav-below" class="navigation">
		<div class="nav-next"><?php previous_posts_link('<img src="/nakamura_wp/wp-content/themes/nakamura/images/common/nav/back.jpg" />') ?></div>
        <div class="nav-previous"><?php next_posts_link('<img src="/nakamura_wp/wp-content/themes/nakamura/images/common/nav/next.jpg" />') ?></div>
        <br class="clear">
	</div><!-- #nav-below -->
<?php endif; ?>
</div>

</section>
<section class="top"> <a href="#"><img src="<?php bloginfo( 'template_url' ); ?>/images/common/top.jpg" alt="ページトップ"></a> </section>
<section class="answer"><img src="<?php bloginfo( 'template_url' ); ?>/images/common/answer-design.jpg"></section>

</div>

<?php get_footer(); ?>
