<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>



<div id="wrap">
<h2><?php the_title(); ?></h2>
<section class="main-photo">
<img src="<?php bloginfo( 'template_url' ); ?>/images/form/photo.jpg" alt="" width="100%"></section>
<section id="form-contents">
<div class="form-contents">
<p class="p_boder">下記フォームより必要事項をご入力の上、「送信内容確認」ボタンを押してください。<br>
お問い合せに対するお答えには時間がかかる場合があります。予めご了承ください。<br>
<span style="color:#FF0004">※</span>は必須事項です。</p>


<?php
/* Run the loop to output the page.
 * If you want to overload this in a child theme then include a file
 * called loop-page.php and that will be used instead.
 */
get_template_part( 'loop', 'page' );
?></div>

</section>
<div class="back">
	<a href="javascript:history.back();"><img src="<?php bloginfo( 'template_url' ); ?>/images/common/back.jpg" alt="戻る" width="45%"></a>
</div>
<section class="top"> <a href="#"><img src="<?php bloginfo( 'template_url' ); ?>/images/common/top.jpg" alt="ページトップ"></a> </section>
<section class="answer"><img src="<?php bloginfo( 'template_url' ); ?>/images/common/answer-design.jpg"></section>

</div>












<?php get_footer(); ?>
