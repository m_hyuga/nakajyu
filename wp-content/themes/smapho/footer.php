<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content
 * after.  Calls sidebar-footer.php for bottom widgets.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?>

<footer>

  <p><img src="<?php bloginfo( 'template_url' ); ?>/images/common/copy.jpg" alt=""></p>
</footer>
<?php
	/* Always have wp_footer() just before the closing </body>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to reference JavaScript files.
	 */

	wp_footer();
?>
<script type='text/javascript' src='<?php bloginfo("stylesheet_directory") ?>/ajaxzip2/ajaxzip2.js'></script>
 <script type="text/javascript">
 //<![CDATA[
 jQuery(function(){
 AjaxZip2.JSONDATA = "<?php bloginfo("stylesheet_directory") ?>/ajaxzip2/data";
 jQuery('#your-zip').keyup(function(event){
 AjaxZip2.zip2addr(this,'your-pref','your-addr');
 })
 })
 //]]>
 </script> 
 
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 975882278;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/975882278/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
<script type="text/javascript" language="javascript">
/* <![CDATA[ */
var yahoo_retargeting_id = 'YU3Y3HYDR3';
var yahoo_retargeting_label = '';
/* ]]> */
</script>
<script type="text/javascript" language="javascript" src="//b92.yahoo.co.jp/js/s_retargeting.js"></script>
</body>
</html>